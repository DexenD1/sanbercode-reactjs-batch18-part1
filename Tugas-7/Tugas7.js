// ===== Soal 1 =====
console.log("===== Soal 1 =====");
console.log("--- Release 0 ---");
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name() {
        return this._name;
    }
    get legs() {
        return this._legs;
    }
    get cold_blooded() {
        return this._cold_blooded;
    }
}
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("--- Release 1 ---");
class Ape extends Animal {
    constructor(name) {
        super(name);
        this._legs = 2;
    }
    yell() {
        console.log("Auooo");
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"
 
var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

console.log();
// END OF SOAL 1

// ===== Soal 2 =====
console.log("===== Soal 2 =====");
class Clock {
    constructor(clock) {
        this._timer = clock.template;
    }

    stop() {
        clearInterval(this._timer);
    }
    start() {
        this.render();
        this._timer = setInterval(()=>this.render(), 1000);
    }
    render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this._timer
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)
        ;
    
        console.log(output);
    }
    
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
clock.stop();

console.log();
// END OF SOAL 2