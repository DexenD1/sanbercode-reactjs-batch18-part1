// Soal 1
console.log("LOOPING PERTAMA");
var i = 1;
while(i <= 20)
{
    if(i%2 == 0)
    {
        console.log(i + " - I love coding");
    }
    i++;
}
console.log("LOOPING KEDUA");
while(i >= 1) 
{
    if(i%2 == 0) 
    {
        console.log(i + " - I will become a frontend developer");
    }
    i--;
}
console.log("\n");
// END of Soal 1

// Soal 2
for(var i = 1; i <= 20; i++)
{
    var output = i.toString().concat(" - ");
    // Jika angka ganjil maka tampilkan 'Santai'
    if(i%2 == 1)
    {
        // Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan 'I Love Coding'
        if(i%3 == 0)
        {
            output = output.concat("I Love Coding");
        }
        else
        {
            output = output.concat("Santai");
        }
    }
    // Jika angka genap maka tampilkan 'Berkualitas'
    else if(i%2 == 0)
    {
        output = output.concat("Berkualitas");
    }

    // Tampilkan output
    console.log(output);
}
console.log("\n");
// END of Soal 2

// Soal 3
var dimensi = 7;
for (let i = 0; i < dimensi; i++)
{
    var output = "";
    for (let j = 0; j <= i; j++)
    {
        output += "#";
    }
    console.log(output);
}
console.log("\n");
// END of Soal 3

// Soal 4
var kalimat="saya sangat senang belajar javascript"
var output = kalimat.split(" ");
console.log(output);

console.log("\n");
// END of Soal 4

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
for (let i = 0; i < daftarBuah.length-1; i++)
{
    for (let j = 0; j < daftarBuah.length-i-1; j++)
    {
        // Jika nilai variabel kiri lebih besar dari kanan
        if(daftarBuah[j] > daftarBuah[j+1])
        {
            // Tukar nilai
            var nilaiSementara = daftarBuah[j];
            daftarBuah[j] = daftarBuah[j+1];
            daftarBuah[j+1] = nilaiSementara;
        }
    }
}
// Tampilkan isi array 'daftarBuah' sampai habis
while(daftarBuah.length != 0) {
    console.log(daftarBuah.shift());
}
// END of Soal 5