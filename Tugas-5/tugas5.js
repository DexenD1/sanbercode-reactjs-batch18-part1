// ===== Soal 1 =====
console.log("===== Soal 1: =====");
function halo() {
    return "Halo Sanbers!";
}
console.log(halo());
// ===== END =====

// ===== Soal 2 =====
console.log("\n===== Soal 2: =====");
function kalikan(num1, num2) {
    return num1 * num2;
}
var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log("Hasil " + num1 + " x " + num2 + " adalah " + hasilKali);
// ===== END =====

// ===== Soal 3 =====
console.log("\n===== Soal 3: =====");
function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby;
}
var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
// ===== END =====

// ===== Soal 4 =====
console.log("\n===== Soal 4: =====");
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var peserta = {
    nama: arrayDaftarPeserta.shift(),
    jenisKelamin: arrayDaftarPeserta.shift(),
    hobi: arrayDaftarPeserta.shift(),
    tahunLahir: arrayDaftarPeserta.shift()
}
console.log(peserta);
// ===== END =====

// ===== Soal 5 =====
console.log("\n===== Soal 5: =====");
var rujak = [
    {
        nama: "strawberry",
        warna: "merah",
        ada_bijinya: "tidak",
        harga: 9000 
    },
    {
        nama: "jeruk",
        warna: "oranye",
        ada_bijinya: "ada",
        harga: 8000
    },
    {
        nama: "Semangka",
        warna: "Hijau & Merah",
        ada_bijinya: "ada",
        harga: 10000
    },
    {
        nama: "Pisang",
        warna: "Kuning",
        ada_bijinya: "tidak",
        harga: 5000
    }
]
console.log(
    "Nama: " + rujak[0].nama +
    "\nWarna: " + rujak[0]["warna"] +
    "\nAda Bijinya: " + rujak[0].ada_bijinya +
    "\nHarga: " + rujak[0]["harga"]
);
// ===== END =====

// ===== Soal 6 =====
console.log("\n===== Soal 6: =====");
var dataFilm = [];
console.log("Sebelum diisi data:");
console.log(dataFilm);

function tambahData(nama, durasi, genre, tahun) {
    var data = {
        nama: nama,
        durasi: durasi,
        genre: genre,
        tahun: tahun
    };
    dataFilm.push(data);
}

console.log("Sesudah diisi data:");
tambahData("Doctor Strange", "120 menit", "Sci-Fi", 2018);
tambahData("Iron Man", "135 menit", "Sci-Fi", 2012);
tambahData("I am not an Impostor!", "10 menit", "Sci-Fi", 2020);
console.log(dataFilm);