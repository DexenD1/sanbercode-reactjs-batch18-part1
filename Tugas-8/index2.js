var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function willReadBooks(time, book) {
    readBooksPromise(time, book)
    .then((result) => {
        console.log("Terpenuhi!");
    }).catch((err) => {
        console.log("Gagal!");
    });
}

books.forEach(element => {
    willReadBooks(
        element.timeSpent,
        element
    );
});