// Soal 1
var kataPertama1 = "saya";
var kataKedua1 = "senang";
var kataKetiga1 = "belajar";
var kataKeempat1 = "javascript";

console.log(
    kataPertama1 + " " +
    kataKedua1.charAt(0).toUpperCase() + kataKedua1.substring(1, kataKedua1.length)+" "+
    kataKetiga1 + " " +
    kataKeempat1.toUpperCase() +
    "\n"
);
// END of Soal 1

// Soal 2
var kataPertama2 = "1";
var kataKedua2 = "2";
var kataKetiga2 = "4";
var kataKeempat2 = "5";

console.log(
    parseInt(kataPertama2) +
    parseInt(kataKedua2) +
    parseInt(kataKetiga2) +
    parseInt(kataKeempat2) +
    "\n"
);
//END of Soal 2

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama3 = kalimat.substring(0, 3); 
var kataKedua3 = kalimat.substring(kalimat.indexOf(" ja"), kalimat.indexOf("t ")+1); // do your own! 
var kataKetiga3 = kalimat.substring(kalimat.indexOf(" it"), kalimat.indexOf("u ")+1); // do your own! 
var kataKeempat3 = kalimat.substring(kalimat.indexOf(" ke"), kalimat.indexOf("n ")+1); // do your own! 
var kataKelima3 = kalimat.substring(kalimat.indexOf(" se"), kalimat.length); // do your own! 

console.log('Kata Pertama: ' + kataPertama3); 
console.log('Kata Kedua: ' + kataKedua3); 
console.log('Kata Ketiga: ' + kataKetiga3); 
console.log('Kata Keempat: ' + kataKeempat3); 
console.log('Kata Kelima: ' + kataKelima3);
console.log();
// END of Soal 3

// Soal 4
var nilai = 66;
var grade = "";
if(nilai >= 80) {
    grade = "A";
} else if(nilai >= 70 && nilai < 80) {
    grade = "B";
} else if(nilai >= 60 && nilai < 70) {
    grade = "C";
} else if(nilai >= 50 && nilai < 60) {
    grade = "D";
} else if(nilai < 50) {
    grade = "E";
}
console.log("Nilai Anda " + nilai + " => " + grade + "\n");
// END of Soal 4

// Soal 5
var tanggal = 16;
var bulan = 3;
var tahun = 2000;
var namaBulan = "";

switch (bulan) {
    case 1:
        namaBulan = "Januari";
        break;

    case 2:
        namaBulan = "Februari";
        break;

    case 3:
        namaBulan = "Maret";
        break;

    case 4:
        namaBulan = "April";
        break;

    case 5:
        namaBulan = "Mei";
        break;

    case 6:
        namaBulan = "Juni";
        break;

    case 7:
        namaBulan = "Juli";
        break;

    case 8:
        namaBulan = "Agustus";
        break;

    case 9:
        namaBulan = "September";
        break;

    case 10:
        namaBulan = "Oktober";
        break;

    case 11:
        namaBulan = "November";
        break;

    case 12:
        namaBulan = "Desember";
        break;

    default:
        console.log("Input 'bulan' tidak valid!");
        break;
}
console.log(tanggal + " " + namaBulan + " " + tahun);
// END of Soal 5